---
title: "Le Webscraping en SHS"
bibliography: references.bib
---

# Introduction

Ce site web pour la *formation au webscraping* est un commun à destination des étudiant.e.s, chercheur.e.s, enseignant.e.s-chercheur.e.s et ingénieur.e.s des sciences humaines et sociales. 

La motivation pour la construction d'un tel support provient avant tout d'un constat collectif. Nous avons remarqué, chacun de notre côté, puis en discutant ensemble entre scientifiques et ingénieur.e.s de plusieurs laboratoires, qu'il y avait une **demande croissante pour la collecte de données en provenance d'Internet**. 
 
Si la demande était d'abord de **comprendre**, petit à petit celle-ci s'est mue en une demande de **formation** pour la mise en pratique au sein de projets de recherche. 

L'hétérogéneité de niveau et de pratique en informatique en Sciences Humains et Sociale rend illusoire la création d'une formation et d'un discours capable d'expliquer la pratique du webscraping à toustes. A cela il faut ajouter la diversité des objectifs poursuivis, avec des défis techniques et méthodologiques parfois très différents. Ainsi, la durée de la campagne de collecte, la multiplicité des sources collectés, le niveau d'intégration et d'harmonisation souhaité des données, leur niveau d'accessibilité plus ou mois directe, le niveau de protection du site web contre le webscraping, sont autant de facteurs propre à complexifier techniquement et méthodologiquement l'expérience de collecte. 

Par exemple, une campagne de collecte sur une journée, sur une seule collection de pages, sur un site peu protégé contre le webscraping et dont l'information à récupérer est directement encodé en html peut être réalisé  en quelques heures sans avoir de compétence techniques préalables. Si en revanche, la collecte se fait sur un an avec plusieurs collections de pages, du contenu dynamique, et sur un site qui a mis en place des protections pour ne pas être moissoné alors il faudra penser à recruter un informaticien pour créer et gérer l'infrastructure et les scripts experts à réaliser.

Pour palier à cette montée en complexité qui va de paire avec les motivations des différents projets mobilisant le webscraping, nous avons préféré construire **ce support comme une base de connaissance capable d'intégrer les connaissances et les expériences des uns et des autres** en un même lieu.

Connu pour ses travaux dans le réseaux des _Software Carpentry_, nous essayons de mettre en oeuvre les principes de Greg Wilson [@Wilson2019] pour construire une **carte des concepts** (_concept map_) qui permettra d'utiliser et de réutiliser cette base et/ou le contenu de cette base dans le but de construire des cours et des trajectoire de lecture en fonction des besoins, des capacités et des motivations du public.

![](img/concept_map.png){fig-align="center"}

Nous avons définis plusieurs profil de personnes apprenant.e.s, auquel nous faisons correspondre différents parcours dans ce schéma global.

::: {.callout-tip title="Qu'est ce qu'un _learner personna_ ?"}

A learner persona consists of : 

- the person’s general background;
- what they already know;
- what they want to do; and
- any special needs they have.

:::

**Camille**

Camille est doctorant.e, iel a fait une prépa lettre avant de rejoindre une licence 3 de géographie. Passionné.e par l'écologie radicale dans toutes ses formes, iel a tout de suite vu l'intérêt de poursuivre dans une formation géomatique pour acquérir l'ensemble des outils et des méthodes propre à l'analyse spatiale des objets (textes, lieux, etc.), des réseaux qui émerge au sein des communautés scientifiques concerné par le déréglement climatique.

Après un master 2 en géomatique, iel est capable de manipuler des SIG aisément pour construire les cartes. La plupart des traitement d'analyses statistiques ont été fait soit avec libre office calc et/ou philcarto. Lors de son stage dans un laboratoire de Géographie Quantitative, iel a pu bénéficier d'un premier stage pour apprendre les bases du langage R à destination d'un usage pour les statistiques. 

Avec l'obtention de sa bourse doctorale pour une thèse en géographie Camille aimerait continuer son apprentissage de R, car de nombreux packages spécialisés, sur le réseaux mais aussi l'analyse de corpus, pourrait bien lui être utile l'année prochaine, une fois les premiers entretiens passés. Lors d'un colloque en humanité numérique, iel a entendu un.e doctorant.e qui avait complété son corpus d'entretien par une collecte de texte sur divers sites Internet, via du Webscraping. A partir de cette collecte, le doctoran.t.e avait réalisé des analyses de corpus plutôt complexes et détaillées qu'iel a pu comparer avec la synthèse de ses entretiens. Depuis l'idée a fait son chemin et Camille aimerait mettre en place un protocole similaire pour collecter et analyser du contenu. Voyant qu'une formation sur la sensibilisation au Webscraping avec R se tenait dans son laboratoire, iel a décidé d'y participer pour en savoir plus, voire même peut être pratiquer tout de suite si c'est possible.

Avec la réalisation de cette fiche [RZine](https://webscraping.gitpages.huma-num.fr/rzine_webscraping/), l'objectif était de répondre au mieux à la demande de Camille. 

Iel va apprendre dans cette formation : 

- qu'est-ce que l'on entend par webscraping, à quoi cela peut servir.
- qu'est ce qu'une page web, du Html pour le fond, du Css pour la forme
- qu'est ce qu'il faut observer quand on charge une page web dans un navigateur
- comment extraire puis nettoyer le contenu d'une page web avec R
- comment systématiser une collecte en construisant ses propres requête et sa propre liste d'URL 

**Alix**

Alix est ingénieur d'étude cartographe. 

