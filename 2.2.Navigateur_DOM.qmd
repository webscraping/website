---
title: "Navigateur / Dom / JS"
bibliography: references.bib
---

Seb (Navigateur = DOM / SHadow Dom ... )

## L'objet "Navigateur" 

Afin d'interagir de façon plus fine avec cette fenêtre sur le monde qu'est devenu le **Navigateur Web**, il est important de voir comment les éléments constitutifs du **World Wide Web** (HTTP / URI / URL) sont manipulés et manipulables par ce dernier.

### Bref historique 

Le **Navigateur Web** est devenu un objet anodin, tellement intégré, voir invisibilisé dans son usage quotidien par des centaines de millions de personnes dans le monde entier qu'on en oublierait l'impact que son apparition, puis sa démocratisation ont eu sur nos sociétés. L'histoire du Navigateur Web est indissociable de l'histoire du Web en tant que tel, c'est pourquoi dans cette section nous reviendrons un peu plus en détail sur l'historique de l'objet, son contexte (invention du World Wide Web) et l'apparition et l'évolution des différentes technologies (HTML, CSS, JS) au coeur de son fonctionnement. 

En effet un Navigateur Web est un objet extremement complexe, dont le code source est majoritairement ouvert,  composé de multiples couches logicielles développé par des acteurs aux intérêts très divers, hérité d'une histoire relativement courte mais riche en rebondissements.

De façon simplifié, on peut imaginer que ces couches logicielles s'organise comme les couches d'un oignon, avec au centre un élément principal, **un moteur de rendu capable de transformer un document fait de HTML, de CSS et de JS en une page web lisible par toustes**. Contrairement à ce que l'on pourrait penser en voyant la diversité de Navigateurs existants (Brave, Safari, Opera, Firefox, Chrome, Edge, etc.) les moteurs de rendu qu'ils encapsulent sont souvent les mêmes. Il existe trois branches historiques de moteurs de rendu, ce qui en fait un formidable ecosystème [^4][^5] avec des fonctionnalités légèrement différentes :

- _Webkit_ et son fork _Blink_ (Safari, Chrome, Opera)
- _Gecko_ (Firefox)
- _EdgeHTML_, un fork de _Trident_ / _MSHTML_ (Edge)

A l'origine de ces trois moteurs on trouve un premier Navigateur, le  [WorldWideWeb/Nexus](https://www.w3.org/People/Berners-Lee/WorldWideWeb.html) développé par **Tim Bernard Lee** au CERN en 1990. Ce dernier permet de visualiser mais aussi d'éditer directement les premières pages _HTML_ du WWW(_World Wide Web_).

Son créateur s'inspire d'objets et de connaissances existantes pour développer sa propre vision d'un système de documentation universel, d'abord utilisable en interne sous une forme centralisé au CERN (_Enquire program_), puis décentralisé ouverte sur le monde (le WWW)[^fWWW]. Comme souvent lors des innovations, la naissance du WWW se fait par la reconfiguration et la combinaison d'éléments et de problématiques déjà existantes, chacune porteuse de son propre passé et devenir. 

A travers les témoignages de Tim Bernard Lee et du W3C - l'instance internationale qui encadre le WWW - ce sont différentes influences qui sont citées : 

- Le _Memex_ de Vannevar Bush, une machine imaginaire [@Vannevar1945]
- Les écrits de J.C.R. Licklider [@Licklider1960] et Robert W. Taylor [@Licklider1968] 
- Le travail de Douglas Englebart sur NLS (_oN-Line System_)  [@Conklin1987] et son rapport [Augmenting Human Intellect](https://dougengelbart.org/content/view/380/) 
- Ted Nelson pour son projet _Xanadu_ et son ouvrage _Literary Machines_ dans lequel il introduit le terme **HyperText** dès 1965 [^fNelson]
- Ray Tomlinson, inventeur de l'email dans sa forme moderne
- Vint Cerf et Bob Kahn, inventeur de TCP/IP, qui permet la transmission de données par paquet sur le réseau Internet.

Dans cette recherche de filiation génétique entre différentes technologies, il est important de garder l'esprit ouvert et ne pas céder à la simplification. On pourra citer par exemple le cas de l'Hypertexte, qui est certe un des axiomes principal dans le système constitutifs du World Wide Web mais dont doit retenir que **tout comme le Web n'est pas Internet, le Web n'est pas non plus l'Hypertexte**[^fHypertexte]. En effet ce n'est qu'une réalisation de l'Hypertexte parmis d'autre, le concept n'ayant pas dit son dernier mot [@Conklin1987; @Barnet2013]. 

Différents prototypes de Navigateurs se développent les années suivantes. Parmis eux on retiendra surtout _Mosaic_, qui va populariser l'objet et ainsi participer à l'explosion du WWW. Ce navigateur propriétaire développé en 1993 au NCSA (_National Center for Supercomputing Applications_) Il est intéressant de noter l'apparition, l'enrichissement des fonctionnalités, mais aussi leur disparition dans le temps. Mosaic par exemple ne permet plus d'éditer les pages comme le permettait WWW.


<!-- ##  Internet -->

<!-- Ces définitions sont basées sur les excellents travaux de vulgarisation produits par Stephen Bortzmayer dans ?? \[REF\], le collectif derrière le Guide d'Autodefense Numérique \[REF\] et l'article Internet du site web [Explained From First Principles](https://explained-from-first-principles.com/internet/) -->

<!-- -   Internet est un réseau d'ordinateurs (noeud) interconnectés (liens) entre eux par des câbles, des ondes, etc. Ces ordinateurs sont différents des ordinateurs domestique ou professionel car ils assurent un pool de services beaucoup plus étendus dans le temps (h24/7j7), dans l'espace. -->

<!-- -   Ces machines communiquent via un équipement commun, une carte réseau, identifié par un numéro de série unique dit **adresse MAC** . -->

<!-- -   Ces ordinateurs peuvent communiquer entre eux, et pour cela ils ont besoin de **Protocoles**, que l'on peut répartir en trois grandes familles : Physiques, Réseaux, Applicatifs. -->

<!--     -   Les protocoles Physiques permettent de régler la coordination, la navigation, la résilience au sein du réseau physique. BGP est un exemple de protocole pour identifier, communiquer les routes et l'état des routes au niveau du réseau physique. -->

<!--     -   Les protocoles Réseaux permettent l'acheminement et la reconstitution des paquets qui circulent sur le réseau physique. C'est le cas par exemple du protocole IP, qui permet de donner une adresse sur le réseau Internet (IP V4/V6) et/ou un nom d'hôte équivalent (DNS). -->

<!--     -   Les protocoles applicatifs sont nombreux, et reposent la plupart du temps sur les protocoles Physiques et Réseaux. C'est dans cette famille qu'il faut ranger le protocole qui supporte le Web (HTTP/S). -->

<!-- Internet est le Web sont donc des choses différentes, or on les confonds souvent dans la langue courante. -->

<!-- http://www.mit.edu/%7Emkgray/net/terminology.html -->

<!-- En général, un site web est un ensemble de pages web reliées entre elle par des liens. -->

<!-- https://wiki.nikiv.dev/web/web-scraping -->

## Du réseau à la mémoire

Lorsqu'on ouvre les outils développeurs (_Web Developper Tools_)de n'importe quel navigateur web (voir [annexe 1](Accéder aux outils developpeur)) on observe parmi les différents onglets qu'il existe un onglet Réseau (_Network_). Dans l'image ci-dessous j'affiche le contenu de cet onglet pour le chargement de la page sur le site [https://lowtechlab.org/fr](https://lowtechlab.org/fr)

![](img/img_low_tech.png){fig-align="center"}

Cet onglet affiche des informations sur les différents flux reçu par le réseau au sein du navigateur lorsqu'on charge une page. On voit qu'ils sont nombreux, il 

- La ligne bleu indique que la page HTML a été récupéré, parsé pour être mis en mémoire au sein d'un objet DOM. L'événement est indiqué par le nom _DOMContentLoaded (DCL)_ A ce stade, les feuilles de styles CSS peuvent ne pas avoir encore été récupérés par le navigateur.

- La ligne rouge indique que tout les éléments indiqué dans le HTML ont été récupéré : feuille de style CSS, scripts Javascript, images, etc. L'événement est indiqué par le nom _loadEvent_

::: {.callout-note collapse="true"}
## Plus de détail encore ...

Pour les plus curieux, il est possible d'aller très très loin dans l'observation des processus mis en oeuvre par le navigateur pour le chargement d'une page. En ouvrant cette fois-ci l'onglet performance, on peut lancer un _profiler_ qui va capturer tout ce qui se passe pendant un laps de temps. Ce type d'outils est souvent utilisé par les développeurs pour trouver des bugs.

![](img/img_low_tech2.png){fig-align="center"}

Sur cette image on voit les différentes catégories et leur étalement dans le temps. On retrouve une partie des phases critique abordés dans la section suivante : Scripting (traitement des scripts JS), Loading (Transformation ), Rendering (Combinaison et application du Layout), Painting (Affichage en pixel). 

:::

## De la mémoire au rendu 

Aussi apelé CRP en anglais ([Critical Rendering Path](https://developer.mozilla.org/en-US/docs/Web/Performance/Critical_rendering_path) c'est la séquence cruciale d'opérations qui permet de combiner la représentation du HTML en mémoire (DOM), la représentation en mémoire du CSS (CSSOM) au sein d'un _Render Tree_. Il est important de noter que ce processus n'implique que des noeuds visible. Les fichiers Javascript sont téléchargés pendant la construction du CSSOM.

La spatialisation et le dimensionnement en taille des éléments de la page se fait dans la phase suivante de _layout_, en fonction de la taille de l'écran. Enfin vient la phase de _painting_ en elle-même, c'est à dire la transformation de l'ensemble en pixel sur l'écran.

Cette séquence d'opération a lieu à différents moments : 

- au chargement initial de la page
- a chaque actualisation du DOM ou du CSSOM est modifié, par exemple par du Javascript.
- a chaque redimensionnement de la fenêtre

Si il y a quelques différences selon les moteurs de rendu, on retrouve plus ou moins le même schéma.

## HTTP / URI / URL

https://www.w3.org/Provider/Style/URI

HyperText Transfer Protocol (HTTP)

## Formatage des documents : HTML et CSS

### HTML 
### CSS
Exemples simple : http://web.simmons.edu/~grovesd/comm244/notes/week4/document-tree.

## Javascript

REF : https://zenodo.org/records/3707008#.X6XOHVMzbDo

## DOM 

Le contenu de la page (HTML) est stocké par le navigateur sous forme d’une hiérarchie de nœud. 

Reference : 
https://www.w3.org/TR/DOM-Level-3-Core/introduction.html

La structure du document en elle même est dénommée `DOM Tree`, mais ce qui nous intéresse le plus c'est le `DOM` (Document Object Model ). Le DOM contient le `DOM Tree`, mais pas seulement, c'est une API[^1] simple et universelle, avec laquelle les programmes et l'utilisateur peuvent interagir (appliquer des styles, déclencher des événements, etc.) via l'intermédiaire d'un langage de script (Javascript[^2] ou autre). 

Historiquement le DOM fait suite à _Dynamic HTML_ ou _DHTML_ dans les Navigateurs (1997, voir https://webdevelopmenthistory.com/1997-the-year-of-dhtml/). Ce standard apparait du fait de la nécessité conjointe, pour CSS, pour JS (1996) de manipuler, requêter le contenu structuré des documents HTML. En effet, sans DOM, le Javascript n'aurait pas d'élément, de page web à manipuler, c'est pourquoi à l'origine ils étaient étroitement liés.

Dénommé ensuite _DOM - level 0_ par le W3C, ce _DHTML_ posait un certain nombre de problème c'est pourquoi le W3C a initié un groupe de réflexion en 1997 pour établir DOM comme une API standard, agnostique, ouverte, et donc utilisable pour n'importe quel langage ayant besoin d'accéder, manipuler un document structuré.

La norme s'est enrichi de nouvelles fonctionnalités au fil du temps, dont certaines comme XPath (2004) qui nous intéressent directement pour le webscrapping. On trouve une spécification détaillé du DOM en cours (DOM v4) sur les  pages du [W3C](https://dom.spec.whatwg.org/)

Enfin, et c'est ce qui va nous intéresser le plus dans un premier temps, grâce à Javascript ou à des langage de requêtes (XPATH selector, CSS selector) il est possible de rechercher et d'extraire les éléments (ou données) qui nous intéressent dans cet arbre. Pour des raisons pédagogiques, nous traitons seulement partie Javascript dans cette section,  XPATH et CSS Selector sont abordés en détail dans la [section](./2.1.HTML_CSS.qmd).

Pour expliquer les différents concepts lié au DOM nous allons partir d'un exemple très simple.

### Structuration en arbre (_tree_)

``` html
<!doctype html>
<html>
  <head>
    <title>Le Webscrapping</title>
  </head>
  <body>
    <h1>Introduction</h1>
    <p>Le Webscrapping est une méthode de récolte automatisé d'un contenu web</p>
    <p>Les outils les plus utilisés sont : </p>
    <ul>
      <li>En Python : <a href="https://scrapy.org/">Scrapy</a></li>
        <li>En R : R <a href="https://cran.r-project.org/web/packages/rvest/index.html">RVest</a></li>
    </ul>
    <p> Voilà, <b> c'est tout !  </b> </p>
  </body>
</html>
```

Ce code source HTML sera représentée dans la mémoire du navigateur sous la forme d'arbre DOM suivante :

```{mermaid}
flowchart
    
    HTML
    
    HTML --> HEAD
    
    HEAD --> TITLE
    
    HTML --> BODY
    
    BODY --> H1 & P1[P] & P2[P] & UL & P3[P]
    
    UL --> LI1[LI] & LI2[LI]
    
    LI1 --> A1[A]
    
    LI2 --> A2[A]
    
    P3[P] --> B
```

Ce [`Tree` (Arbre)](https://dom.spec.whatwg.org/#trees) est un `Document` fait d'un ensemble de `Node` (Noeuds) générique qui partage des propriétés communes. Ces noeuds ont des relations de hierarchies (`parent`, `children`, `descendant`, `ascendant`, `next sibling`, `previous sibling` ), ce qui qui permet de naviguer, circuler dans l'arbre assez aisément. 

:::callout-note
La structure de donnée de type [arbre en informatique](https://en.wikipedia.org/wiki/Tree_(data_structure)) est une structure récursive très courante en informatique. Par récursif il faut entendre qu'un Noeud de l'arbre peut contenir soit : 0 Noeud (on parle `leaf` ou `external nodes`), soit un ensemble de Noeuds fils (`internal nodes`). Les noeuds sont reliés entre eux par des branches. 

La terminologie reprise pour décrire le DOM ne fait que reprendre une grande partie de la terminologie courante pour ce type de structure de donnée.
:::

Si on regarde plus en détail, ces `NODE` peuvent être de nature différentes, ils possèdent en effet des types (`Node.nodeType`) qui indiquent en quoi ceux-ci se différencient : `ELEMENT_NODE`, `TEXT_NODE`, `DOCUMENT_NODE` pour ne citer que les plus pertinents dans notre cadre. 

- Un  `NODE` de sous-type `TEXT_NODE` signifie qu'il s'agit de texte et rien d'autre.
- Un `NODE` de sous-type `ELEMENT_NODE` signifie qu'il s'agit d'une balise HTML

De part leur nature récursive, ces `ELEMENT_NODE`  peuvent soit ne rien contenir, soit contenir des Noeuds fils (`children`).

Dans l'arborescence telle que calculée par [live DOM viewer](https://software.hixie.ch/utilities/js/live-dom-viewer/saved/12270), les `ELEMENT_NODE` sont en violet, et les `TEXT_NODE` en gris.

![](img/dom_viewer.png){fig-align="center"}

Pour y voir plus clair, nous allons faire quelques manipulations de l'arbre avec l'aide de Javascript.

### Manipulation de l'arbre avec JS

Le premier langage capable de manipuler le DOM est Javascript, avec de simple requête il devient possible d'extraire, d'ajouter, de modifier, de supprimer des éléments ou des attributs HTML.

Les codes préséent dans cette section peuvent tous être executé dans une Sandbox [MSDN]( https://developer.mozilla.org/fr/play).

::: {.callout-note collapse="true"}
## Manipuler des Objets avec JS en 2 mots

Cette partie donne quelques éléments principaux pour comprendre la suite, le lecteur plus curieux pourra se référer à la [section Javascript](./2.4.Javascript.qmd).


Javascript, comme beaucoup d'autres langages de programmation (R, Python) s'appuie sur le paradigme Objet (Programmation Orienté Objet ou POO) pour structurer l'information de ces programmes. 

En Javascript un objet est fait d'un ensemble de clefs et de valeurs qui sont représentée par le couple `clef:valeur` 

Par exemple l'objet `myBook`  contient les **attributs** `author`, `title`, `year` : 

```
const myBook = {
  author: "Sebastien R", 
  title: "boobook",
  isbn: "31223212",
  year: 1969,
};
```

Les attributs peuvent être de tout type : chaine de caractère, numérique, tableau, objet, etc. 

Pour accéder aux attributs de l'objet, on utilise une notation avec un point (`myBook.author` ) ou un crochet (`myBook["author"]`).

Il est aussi possible de définir des **fonctions** à nos objets. Dans cet exemple on définit deux fonctions `addBook` qui ajoute l'objet livre passé en paramètre (`newBook`) à l'attribut `books`, un tableau pour le moment vide définit ainsi en javascript :  `[]` . 


```javascript
const myBook = {
  author: "Sebastien R", 
  title: "boobook",
  isbn: "31223212",
  year: 1969,
};

const myLibrary = {
  name: "L'échiquier", 
  books: [],
  
  addBook: function( newBook ){ 
    this.books.push(newBook);
    console.log("pushed a new book")
  },
  
  countBook: function() {
    console.log (this.books.length);
  },
  
};

```

Les fonctions et méthodes peuvent être appelée en utilisant la notation indicée suivante : `nomdelobjet.nomdelafonction(paramètres)`.

```javascript
myLibrary.countBook();
myLibrary.addBook(myBook);
myLibrary.countBook();
```

Les attributs et les fonctions sont partout en Javascript, on les utilise parfois même sans le savoir. Par exemple pour afficher le contenu d'une variable, d'un attribut dans la console on utilise en général la fonction `console.log(...)`, ce qui revient finalement *in fine* à appeler la méthode `log(...)`  de l'objet console.

```javascript
console.log(myBook.author)
```

Et enfin il est possible d'accéder et de récupérer des objets stocké dans des objets via des tableaux. On modifie notre objet `myLibrary` pour qu'il retourne le livre correspondant au bon ISBN grâce à la fonction `getABook()`

```javascript

//  ... meme code que précédemment ...
 
const myLibrary = {
  
 //  ... meme code que précédemment ...
  
  getABook: function ( isbnToFound ) {
    let item = this.books.filter(b => b.isbn == isbnToFound);
    return item.pop()
  }
};

myLibrary.addBook(myBook);
var abook = myLibrary.getABook("31223212");
console.log(abook.author);
```

Renvoie le résultat suivant : 

```console
pushed a new book
"Sebastien R"
```

Mais nous aurions pu également directement accéder au tableau depuis l'attribut, sans passer par une fonction filtrant le catalogue de livres `books`, à condition de connaitre la position du livre ... 

```javascript
console.log(myLibrary.books[0].author);
```

Sachant cela, nous avons les éléments de compréhension nécessaire pour comprendre comment manipuler les différents éléments de notre arbre. 

:::

Pour récupérer l'ensemble des eléments HTML sous un Noeud, on peut utiliser la fonction `children`. Les éléments sont récupérés sous la forme d'une collection `HTMLCollection`  dans laquelle on peut récupérer les élements soit en utilisant la fonction `item(x)`, soit directement en utilisant la notation crochet `[x]`

```javascript
const root = document.children;
console.log(root[0].children.length); // renvoie 2 éléments : Head, Body
console.log(root[0].children[1].children); // renvoie 5 éléments : h1, p, p, ul, p
let save_p = root[0].children[1].children[4]; // sauvegarde l'element p 
```

Si on questionne l'élément `save_p`, qui contient dans notre graphe à la fois un Noeud texte  ("Voilà") et un autre Noeud balise de type b ("c'est tout ! ") avec la fonction children().

On se rend compte que celle-ci nous renvoie seulement les noeuds de type `Node.Node_Element`. Pour récupérer l'ensemble des Nodes, il faut faire appel à une autre fonction `childNodes`.

```javascript
console.log(root[0].children[1].children[4].childNodes); // renvoie une NodeList avec un Node.Text_Node #text et un Node.Element_Node b 
console.log(root[0].children[1].children[4].childNodes[1].childNodes[0].textContent); // renvoie le contenu du Node.Text_Node contenu dans b
```

::: {.callout-info collapse="true"}
On remarquera ici la lourdeur des commandes pour se déplacer manuellement dans l'arbre. Nous utilisons ces fonctions ainsi pour des raisons pédagogique.
:::

Pour accéder aux éléments situé sur la même ligne hierarchique, ou dit plus simplement, existant dans la même collection de Noeuds, il est possible d'utiliser les fonctions `nextElementSibling` et `previousElementSibling`
```javascript 
let save_ul = root[0].children[1].children[3]; // renvoie l'element ul 
console.log(save_ul.children[0].nextElementSibling.children[0].href) // renvoie https://cran.r-project.org/web/packages/rvest/index.html 
```

::: {.callout-tip }
Les collections renvoyé par le DOM (`HTMLCollection`, `NodeList`, etc. ) ne sont pas des `Array` classique tel qu'on peut les avoir en Javascript. Pour les transformer en objet de type `Array`, et bénéficier de toutes les fonctions utilitaires qui vont avec, il faut les convertir avec la fonction `Array.from()`
:::

Si nous souhaitons récupérer le parent d'un élément il suffit d'appeler `parentElement` ou `parentNodes` si on souhaite avoir les différents type de noeuds :

```javascript
console.log(root[0].children[1].children[4].childNodes[1].parentElement); // renvoie le parent de b, à savoir p
```



Pour récupérer l'ensemble des `Elements` de type `p` nous pouvons nous appuyer sur la méthode `getElementsByTagName()`: 

``` js
let searchedTags = Array.from(document.getElementsByTagName("p"));
searchedTags.forEach((element) => console.log(element.textContent));
```

Ce code permet de lister le contenu d'un objet `HTMLCollection` avec une boucle. Une `HTMLCollection` est un  tableau composé d'élements `HTMLElement` de type `<p>` ou `HTMLParagraphElement`. Ce qui affiche le contenu suivant.

```default
Le Webscrapping est une méthode de récolte automatisé d'un contenu web
Les outils les plus utilisés sont :
Voilà, c'est tout !
```

Il est aussi possible d'ajouter des nouveaux `Element` à cet arbre de façon dynamique, par exemple si on désire ajouter un nouvel élement `<p>` à notre document HTML : 

``` js
let searchedTags = Array.from(document.getElementsByTagName("p"));
let newElement = document.createElement("p");
newElement.textContent = "En fait j'avais oublié quelque chose !";
let lastElementP = searchedTags[searchedTags.length - 1];
lastElementP.parentNode.insertBefore(newElement, lastElementP);
```

Nous allons refaire cette manipulation depuis le navigateur : 
- Ouvrez la page [toy statique](https://webscraping.gitpages.huma-num.fr/website/toy_statique.html).
- Ouvrez les outils `Web Developper Tools` : ceux-ci sont accessibles l'on fait un clic droit n'importe ou sur la page, puis en selectionnant l'option  `Inspecteur`
- Mettez vous sur l'onglet `Console` de cette nouvelle fenêtre ouverte en bas de votre écran.
- Copiez/Collez le bout de code javascript précédent et appuyez sur `Entrée`

![](img/toy_simple_source_js.png){fig-align="center"}

Normalement votre contenu HTML devrait changer ainsi : 

![](img/toy_simple_source_ajout_texte.png){fig-align="center"}

Maintenant nous allons inspecter le code source du document via l'outil disponible avec un clic droit, pour visualiser la page source > `View Page Source`

![](img/toy_simple_source_src.png){fig-align="center"}

Ce qui nous ainsi de constater un élément important, en effet **le code source HTML n'a pas changé** : 

C'est normal, le code source affiche par cette commande est celui envoyé par le SERVEUR WEB, or nous avons modifié cette page en faisant usage de Javascript côté CLIENT (Navigateur).

![](img/toy_simple_source_src_not_updated.png){fig-align="center"}

Si on se replace sur l'onglet `inspecteur` des `Web developper tools`, le code reflète bien les modifications qui ont pu être faite. 

Cette différence, entre ce qui est affiché dans le code source envoyé par le SERVEUR, et le code source tel qu'il est stocké en mémoire et rendu par le navigateur côté CLIENT est importante, car la majorité des sites web d'aujourd'hui jongle entre ces deux approches, et cela pour différentes raisons que l'on évoquera dans la section suivante. 

::: {.callout-important}
Le code source et donc le fichier de la page HTML original n'est pas modifié par le code, c'est seulement sa représentation en mémoire qui est modifié. 
:::

TODO : Fournir un exemple simple, de bout en bout : HTML, DOM, XPATH , s'appuyer la dessus : https://software.hixie.ch/utilities/js/live-dom-viewer/ et le doc de référence : https://dom.spec.whatwg.org/#introduction-to-the-dom
https://phuoc.ng/collection/html-dom/

## Web *Synchrone* et  *Asynchrone*

Si nous faisons abstraction du flou qui accompagne l’usage du terme “Web 2.0”[^3], et que l’on se concentre plus sur les nouveautés techniques qui sont apparus dans cette période des années 2000, alors il faut évoquer la remise en cause du schéma classique synchrone entre client et serveur. 

### XHR / Fetch / Axios / Websocket

La démocratisation des technologies regroupées sous le nom d’AJAX (Asynchronous JavaScript And XML) permet l’échange d’informations entre le navigateur et le serveur sans que le serveur n’ait besoin de renvoyer l’entièreté de la page (circulation asynchrone des données). 

Concrétement c'est ce que vous pouvez parfois observer quand le réseau est très ralenti ou dégradé, une partie seulement de la page s'affiche, puis le contenu apparait peu à peu, le temps que les requêtes réseau envoyé depuis le navigateur recoivent une réponse sous forme de contenu de la part du serveur.   

Historiquement il s'agissait d'un flux au format XML, l'acronyme est resté mais il est bien sur possible d'envoyer toute forme d'informations par ce biais, peu importe la nature de celle-ci.

![Un navigateur envoie une première requête à t0 et reçoit une réponse au format HTML à t1. C’est une étape synchrone obligatoire. Ensuite, à partir de t2, la modification de la page HTML se fait via des appels javascript (requêtes XHR) qui renvoient des données en JSON/XML, de façon asynchrone. ](img/lamp2.png){fig-align="center"}

Le serveur HTTP fournit la partie html/css/js (“frontend”) qui s’affiche dans le client navigateur, mais une partie de ce html/css/js, peut être à la fois générée par le serveur (“backend”), et a posteriori par le navigateur de façon dynamique en fonction de requêtes spécifiques écrites en Javascript, déclenchées par les actions utilisateurs (clic, focus, etc.) ou le développeur (chargement du contenu, intégration d'autres pages, etc.). Ainsi, le code source de la page web récupéré à un instant t ne contient pas nécessairement l’information définitive. Cette capacité est utilisée pour diverses raisons (rapidité d’affichage, interactivité, etc.) et sert aussi de support à des requêtes en provenance d’API.

L'API Javascripts qui a rendu cette asynchronicité possible courant 1999 est `XMLHttpRequest()` [^fXHR] ou `XHR`. Depuis les technologies web se sont beaucoup diversifié, et si les deux acteurs principaux de ce dialogue (CLIENTS / SERVEURS) sont toujours présents, les possibilités de dialogue entre les deux s'est encore considérablement enrichi, au risque de brouiller les pistes même pour les [experts dans ce domaine](https://web.dev/articles/rendering-on-the-web)

Par exemple [Fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API) est une nouvelle API introduite en 2015 qui vient supplanter XHR par sa flexibilité et l'ajout de nouvelles possibilités. Le principe global, c'est à dire l'établissement d'un flux asynchrone entre client et serveur, reste le même. 

La dénomination SPA, ou [_Single Page Application_](https://en.wikipedia.org/wiki/Single-page_application) est le terme global qui renvoie vers l'usage systématisé de ce type de fonctionnement asynchrone. En général, on reconnait assez facilement une page de type SPA au fait que le contenu de la page web évolue, se charge sans que l'on ait besoin de rafraichir ou de changer de page. 

Une très grande partie des outils - ou plutôt framework de par leur complexité -  utilisés aujourd'hui par les développeur web sont en lien avec le développement de techniques de chargement de contenu dynamique. 

C'est aussi en oposition de ces frameworks, de plus en plus complexes à maitriser, que les générateurs de sites web dit **statiques** (SSG pour _Static Site Generator_ ) sont réapparus dans le paysage dès 2000. Au début il s'agissait essentiellement d'outils pour faire du blog, comme MovableType, Nanoc, en fin Jekyll qui a largement démocratisé l'idée en 2008 via l'usage facilité de l'outil dans Github. Depuis, ce retour à une certaine forme de simplicité s'est largement diffusé à tous les formats de sites web, avec l'arrivée de CMS statique et finalement la création d'hybride complet, comme [Astro.js](https://astro.build/), qui couvre l'ensemble du gradient entre complétement statique et complétement dynamique dans leur fonctionnement.

Par rapport à la pratique du webscraping ces nouvelles techniques peuvent amener du positif mais aussi du négatif. 

Sur **le plan positif**, beaucoup de pages web s'appuie sur des requêtes XHR vers une API REST pour alimenter ce contenu dynamique. C'est le cas par exemple du site web FlightRadar. Très concrétement cela veut dire qu'une requête http(s) est directement formulé dans le code du site web pour récupérer de la donnée structurée (csv,xml,json) qui va alimenter les éléments HTML de la page. 

Sur le **plan négatif**, le fait que le contenu ne soit pas forcément accessible au chargement de la page, et donc absent du code source HTML renvoyé par le serveur, fait qu'il faut user de différents stratagèmes pour forcer le chargement de ce contenu et pouvoir le récupérer.

Plusieurs exemples traitant des points positifs et négatifs sont abordés en détail [dans la section spécifique sur les API](./3.4.API_XMLHttpRequest.qmd). 

### Virtual Dom, Shadow DOM

Avec l'arrivée de cette capacité à charger du contenu dynamique avec Javascript de nouvelles techniques de manipulation du DOM sont apparus. On se focalisera sur deux d'entres elles, le Virtual DOM, et le Shadow DOM en essayant de donner des exemples pratiques.

#### Virtual DOM

Le terme Virtual DOM, VDOM est générique dans le sens ou il encapsule un principe qui peut ensuite être implémenté diférément par les différents framework Javascript. En soit ce n'est pas une fonctionnalité du Navigateur, mais un processus qui est un moyen pour une fin[^vDomSvelte].

Il faut comprendre qu'en travaillant en asynchrone, et en chargeant du contenu de façon dynamique et continue en échangeant avec le serveur, nous mettons à jour le DOM qui est stocké en mémoire. Or il faut bien mettre à jour la version qui est actuellement rendu dans le navigateur si on veut que le DOM en mémoire reflète le DOM tel qu'il est affiché. C'est pour cela qu'il s'agit plus d'un moyen pour une fin, il s'agit avant tout de synchronisé ce qui est en mémoire avec ce qui est affiché. 

Le principe est assez simple, il s'agit de comparer le DOM tel qu'il est actuellement rendu par la page avec le DOM tel qu'il est stocké en mémoire. C'est ce que l'on appelle de façon générique en terme informatique un _diff_ 

::: {.callout-note collapse="true"}
## Le principe du diff

![](img/toy_diff_exemple.png){fig-align="center"}

Dans cet exemple en HTML vous avez à gauche le contenu avant modification, à droite le  contenu après modification. Le résultat du _diff_ entre ces deux contenu est le suivant :

- l'ajout de `manuelle et/ou` à la ligne 8
- l'ajout de  `<a href="https://scrapy.org/">Scrapy</a>` à la ligne 11
- l'ajout de  `<a href="https://cran.r-project.org/web/packages/rvest/index.html">RVest</a>` à la ligne 12

:::

Le résultat de cette opération nous indique qu'elle partie du document à changé. Les frameworks Javascript se servent de ce résultat pour mettre à jour une seule partie de l'arbre DOM dans une opération dites de _reconciliation_, ce qui permet à la fois de consommer moins de ressources et d'être beaucoup plus rapide que si on mettait à jour l'ensemble de l'arbre à chaque mise à jour des données.

#### Shadow DOM

- Firefox 123 , fevrier 2024 : Declarative Shadow Dom
https://developer.chrome.com/docs/css-ui/declarative-shadow-dom
https://www.mozilla.org/en-US/firefox/123.0a1/releasenotes/#note-789984
::: {.callout-important}


:::

[^vDomSvelte]: Ce point de vue est tiré de la page d'un développeur du framework [Svelte](https://svelte.dev/blog/virtual-dom-is-pure-overhead) pour critiquer une idée reçu qui vont de paire avec l'apparition de React, un autre framework très utilisé.

[^fXHR]: L'apparition de ces objet XMLHttpRequest est relaté par Alex Hopmann sur son blog conservé par [Internet Archive](https://web.archive.org/web/20070323120943/http://www.alexhopmann.com/xmlhttp.htm)

[^fNelson]:  Ted Nelson théorisera aussi les notions d'HyperMedia, de transclusion, de virtuality, d'intertwingularity, etc. 

[^fWWW]: Le premier proposal est en ligne sur cette page https://www.w3.org/History/1989/proposal.html
[^fHypertexte]: Si la vision des promoteurs de Xanadu a très largement essaimé, le système et ses rouages prévu, trop complexes pour leur temps, n'ont jamais été achevé complétement. Pour certains c'est aussi une question de copyright, le code  source des prototypes (libéré sous le nom de Udanax Green et Gold ici http://udanax.xanadu.com/) n'a été libéré que très tardivement en 1999, le projet original Xanadu restant encore sous secret. Les spécifications de 1984 ont été retrouvé et libéré sur ce site : https://sentido-labs.com/en/library/201904240732/Xanadu%20Hypertext%20Documents.html. Une destinée asez similaire à celle qu'a connu Charles Babbage avec ses machines... 

    > Today's one-way hypertext-- the World Wide Web-- is far too shallow.  The Xanadu  project foresaw world-wide hypertext decades ago, and endeavored to create a much deeper system.  The Web, however, took over with a very shallow structure. 
    
    Src: https://xanadu.com/xuTheModel/index.html

[^4]: voir les deux pages https://css-tricks.com/the-ecological-impact-of-browser-diversity/#article-header-id-0 et https://css-tricks.com/browser-engine-diversity/ pour comprendre l'intérêt de maintenir cette diversité.
[^5]: une diversité d'acteurs et d'objectifs dont on trouve un récit ici, dans l'historique du développement de CSS GRID, un standard développé par des acteur et des fonds extérieurs à la plateforme  : https://bkardell.com/blog/Beyond.html

[^6]: Voir https://www.w3.org/History/19921103-hypertext/hypertext/WWW/MarkUp/Tags.html

[^7]: Voir https://www.w3.org/People/howcome/p/cascade.html

[^9]: Une API est une interface de programme pensée pour faciliter le dialogue entre serveur / serveur & serveur / client. Les développeurs spécialisés dans les aspects serveurs mettent généralement à disposition une API simplifiée et sécurisé pour la récupération des données à afficher (souvent au format structuré XML ou JSON), ce qui facilite et sécurise le travail des développeurs “frontend” en fixant la nature et le périmètre des données échangées (figure 2).
[^10]: 
     > Gérard Genette provided the following definition: “Hypertextuality refers to any relationship uniting a text B (which I shall call the hypertext) to an earlier text A (I shall, of course, call it the hypotext) upon which it is grafted in a manner that is not that of commentary” (Genette 1982, English translation 1997, 5). 
[^11]: La relation historique qui lie au départ DOM à Javascript est bien explicité dans l'introduction de la très complète documentation Mozilla MDN
[^12]: S’il y a bien eu pour certain un “tournant”, et pour d’autre une “évolution” sur le plan culturel et technique, l’appellation de web “2.0” apparait avant tout pour des considérations marketings.


